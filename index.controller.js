const fs = require('fs');
const lineReader = require('readline');

var app = new Vue({
	el: '#app',
	data: {
		text: [],
		ignore: "",
		maxNumber: 0,
		ignoredWords: [],
		filterType: 0,
		inputDisabled: true
	},
	methods: {
		onFileChange: function(e) {
			var vueText = this.text = [];
			var files = e.target.files || e.dataTransfer.files;
			if (!files.length) {
				return;
			}
			var readLine = lineReader.createInterface({
				input: fs.createReadStream(files[0].path)
			});
			readLine.on('line', function(line) {
				vueText.push({
					txt: line,
					displayText: line,
					class: false
				});
			});
			this.inputDisabled = false;
		},
		filterByLineLength: function() {
			for (var i = 0; i < this.text.length; i++) {
				if (this.maxNumber > 0 && this.text[i].displayText.length > 0 && this.text[i].displayText.length > this.maxNumber) {
					this.text[i].class = true;
					this.text[i].errorText = "Length: " + this.text[i].displayText.length;
				} else {
					this.text[i].class = false;
					delete this.text[i].errorText;
				}
			}
		},
		ignoreWord: function() {
			this.ignoredWords.push(this.ignore);
			this.ignore = "";
			filterIgnoredWords();
			this.filterByLineLength();
		},
		deleteIgnoredWord: function(index) {
			this.ignoredWords.splice(index, 1);
			filterIgnoredWords();
			this.filterByLineLength();
		}
	}
});

function filterIgnoredWords() {
	for (var i = 0; i < app.text.length; i++) {
		app.text[i].displayText = app.text[i].txt;
		for (var j = 0; j < app.ignoredWords.length; j++) {
			var reg = new RegExp(app.ignoredWords[j], "g");
			app.text[i].displayText = app.text[i].displayText.replace(reg, "");
		}
		app.text[i].displayText = app.text[i].displayText.trim();
	}
}
